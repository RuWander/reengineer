---
title: "DevOps"
icon: "fa-file"
weight: 4
---

We believe that devOps is a mindset and not just tooling. Such a mindset will enable:

 - One-team-thinking.
 - Smoother release cycles.
 - Standardized tech stacks.
 - Feedback loops.
 - Visibility and reporting across the entire SDLC.



